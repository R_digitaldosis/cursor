$(document).ready(function(){


	let mouseMoveFunc = (evt) => {
		let x = evt.pageX;
  		let y = evt.pageY;

  		/*$('.value--X').attr('value',x);
  		$('.value--Y').attr('value',y);*/

  		$(".wrapper__cursor").show();
  		TweenLite.to($(".wrapper__cursor"), 0.5, {x:x, y:y})
	}

	let enter = () => {
		TweenLite.to($(".wrapper__cursor"), 0.3, {scaleX:1.5, scaleY:1.5, opacity:1, zIndex:1, delay: .1})
	}

	let out = () => {
		TweenLite.to($(".wrapper__cursor"), 0.3, {scaleX:0, scaleY:0, opacity:0, zIndex:-1})
	}

	$(".wrapper__area").mousemove(mouseMoveFunc);
	$(".wrapper__area").mouseenter(enter);
	$(".wrapper__area").mouseleave(out);

	$('.wrapper__area').on("mousedown", function() {
	  $(this).addClass('blackmoon')
	  TweenLite.to($(".wrapper__cursor"), 0.4, {scaleX:3.5, scaleY:3.5})
	  TweenMax.staggerTo($(".external-flower-path"), 1, {fill:'red'}, 0.05)
	});
	$('.wrapper__area').on("mouseup", function(){
	  $(this).removeClass('blackmoon')
	  TweenLite.to($(".wrapper__cursor"), 0.4, {scaleX:1.5, scaleY:1.5})
	  TweenLite.to($(".external-flower-path"), 0.4, {fill:'black'})
	});

})