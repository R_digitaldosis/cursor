'use strict';
// global requires
var gulp = require('gulp'),

    del = require('del'),
    concat = require('gulp-concat'),
    svg = require('gulp-svg-sprite'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    cssnano = require('gulp-cssnano'),
    browserSync = require('browser-sync').create(),
    sass = require('gulp-sass');



var config = require('./config.json'),
path = 'dist';

// cleanup /dist folder
gulp.task('svg', function() {
    return gulp.src('src/img/icons/*.svg')
	.pipe(svg({
        mode: {
            symbol: {
                inline: true
            }
        }
    }))
	.pipe(gulp.dest('src/img/svg'));
});

// cleanup /dist folder
gulp.task('clean', function() {
    return del.sync(path);
});

gulp.task('css-vendors', function() {
    return gulp.src([
            // 'node_modules/flickity/dist/flickity.min.css',
            // 'node_modules/air-datepicker/dist/css/datepicker.min.css'
        ])
        .pipe(browserSync.stream())
        .pipe(concat('vendors.min.css'))
        .pipe(gulp.dest(path+'/css'));
});

// moves all html from /src to /dist
gulp.task('markup', function() {
    // return gulp.src(['src/**/*.json', 'src/**/*.ico'])
    return gulp.src([
        'src/**/*.json',
        'src/**/*.css',
        'src/**/*.js',
        'src/**/*.ico',
        'src/**/*.php',
        'src/**/*.html',
        'src/**/*.twig',
        'src/**/*.png',
        'src/**/*.jpg',
        'src/**/*.gif',
        'src/**/*.svg',
        'src/**/*.mp4',
        'src/**/*.webm'
    ])
    .pipe(browserSync.stream())
    .pipe(gulp.dest(path+'/'));
});
// moves all fonts from vendors/src to /dist
gulp.task('fonts', function () {
	return gulp.src(['src/fonts/**/*.*'])
		.pipe(browserSync.stream())
		.pipe(gulp.dest(path+'/fonts'));
});
// moves all fonts from vendors/src to /dist

// moves all images from /src to /dist
gulp.task('images', function() {
    return gulp.src(['src/img/*.*', 'src/img/*/*.*'])
        .pipe(browserSync.stream())
        .pipe(gulp.dest(path+'/img'));
});

// vendors
gulp.task('vendors', function() {
    return gulp.src([
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/gsap/src/minified/TweenMax.min.js',
        ])
        .pipe(plumber())
        .pipe(concat('vendors.min.js'))
        .pipe(gulp.dest(path+'/js'));
});


// scripts
//gulp.task('scripts', function() {
//    return gulp.src(['src/**/*.js'])
//
//    .pipe(plumber({
//        errorHandler: onError
//    }))
//
//    .pipe(concat('app.js'))
//        .pipe(browserSync.stream())
//
//    .pipe(gulp.dest(path+'/js'));
//});

console.log('PORT-----------', config.port);
gulp.task('browser-sync', function() {

    return browserSync.init({

        //proxy: config.proxy,
        server: {
            baseDir: 'dist',
            index: 'index.html'
        },
        //port: config.bsPort,
        open: true
    });

});



//Compile scss files
gulp.task('sass', function() {
    var postcss = require('gulp-postcss');
    var autoprefixer = require('autoprefixer');
    // require('postcss-flexibility')

    var processors = [autoprefixer({
        browsers: ['last 2 versions', 'ie >= 10'],
        cascade: false,
        grid: true
    })];
    gulp.src('scss/**/*.scss')
        .pipe(sass({
            outputStyle: 'compressed',
            // includePaths: ['node_modules/susy/sass']
        }).on('error', sass.logError))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cssnano({
            zindex: false,
            reduceTransforms: false
        }))
        .pipe(postcss(processors))
        .pipe(browserSync.stream())
        .pipe(gulp.dest(path+'/css'));

});

// watch files for live reload
gulp.task('watch', function() {
    // gulp.watch('src/css/**/*.css', ['css']);
    gulp.watch('src/js/**/*.js', ['markup']);
    gulp.watch(['src/**/*.json', 'src/**/.htaccess'], ['markup']);
    gulp.watch('scss/**/*.scss', ['sass']);

    gulp.watch([
        'src/**/*.html',
        'src/**/*.php',
        'src/**/*.twig'
    ], ['markup']).on('change', browserSync.reload);
});

// tasks
gulp.task('build', ['clean', 'vendors', 'css-vendors', 'markup', 'fonts', 'images', 'sass']);
gulp.task('rocks', ['build', 'browser-sync', 'watch']);
